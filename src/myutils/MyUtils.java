package myutils;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Period;

/**
 *
 * @author monika
 */
public class MyUtils {

    /**
     *
     * @param string String cadena que es vol invertir
     * @return cadena invertida (null per cadenes nulls).
     */
    public static String inverteix(String string) {
        StringBuilder invertir = new StringBuilder(string);

        return (string != null)?invertir.reverse().toString():"null";
    }

    /**
     *
     * @param day int dia del naixement
     * @param month int mes del naixement
     * @param year int any del naixement
     * @return edat de la persona, per edat>150 retorna -1, per dates
     * impossibles retorna -2
     *
     */
    public static int edat(int day, int month, int year) {
        boolean validDate = true;

        try {
            LocalDate.of(year, month, day);
        } catch (DateTimeException e) {
            validDate = false;
        }

        Period fecha = Period.between(LocalDate.of(year, month, day), LocalDate.now());
        int resultat = (validDate) ? (fecha.getYears() > 150) ? -1 : fecha.getYears() : -2;

        return resultat;
    }

    /**
     *
     * @param num número del que es calcula el factorial
     * @return retorna el factorial d'un número. Si el numero es negatiu retorna
     * -1.
     */
    public static double factorial(double num) {
        double factor = 1;

        for (int i = 1; i <= num; i++) {
            factor *= i;
        }

        return (num <= 0) ? -1 : factor;

    }
}
