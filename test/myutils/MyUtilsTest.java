package myutils;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author monika
 */
public class MyUtilsTest {

    public MyUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {

    }

    /**
     * Test of inverteix method, of class MyUtils.
     */
    @Test
    public void testInverteix() {
        String[] arrayInvertir = {"hola", "olah", "Hola Mundo"};
        System.out.println("Test Invertir");

        assertFalse(arrayInvertir[1].equals(MyUtils.inverteix(arrayInvertir[0])));
        assertTrue("odnuM aloH".equals(MyUtils.inverteix(arrayInvertir[2])));

        String result = MyUtils.inverteix(arrayInvertir[2]);
        assertEquals("odnuM aloH", result);

        for (int i = 0; i < arrayInvertir.length; i++) {
            System.out.println("Result: " + MyUtils.inverteix(arrayInvertir[i]));
        }
    }

    /**
     * Test of edat method, of class MyUtils.
     */
    @Test
    public void testEdat() {
        System.out.println("Test edat");

        int[][] fecha = {
            {21, 3, 1620},
            {2, 22, 1999},
            {05, 10, 1983}
        };

        assertFalse(MyUtils.edat(fecha[0][0], fecha[0][1], fecha[0][2])
                == (MyUtils.edat(fecha[2][0], fecha[2][1], fecha[2][2])));

        assertTrue(37 == MyUtils.edat(fecha[2][0], fecha[2][1], fecha[2][2]));

        int result = MyUtils.edat(fecha[2][0], fecha[2][1], fecha[2][2]);
        System.out.println("Resultat: " + result);
        assertEquals(37, result);
    }

    /**
     * Test of factorial method, of class MyUtils.
     */
    @Test
    public void testFactorial() {

        double result = 0;
        int numero[] = {2, 5, 7, 9};

        for (int i = 0; i < numero.length; i++) {
            System.out.println("Numero: " + numero[i] + ", Factorial: " + MyUtils.factorial(i));
        }

        assertTrue(MyUtils.factorial(numero[2]) != MyUtils.factorial(numero[1]));
        assertFalse(MyUtils.factorial(numero[2]) != MyUtils.factorial(numero[2]));
        result = MyUtils.factorial(numero[2]);
        System.out.println("Resultado testFactorial: \n" + result);
        assertEquals(5040, result, 0.0);
    }
}
